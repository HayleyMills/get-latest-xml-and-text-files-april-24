#!/usr/bin/env python3

"""
Python 3: Web scraping using selenium
    - Download tv.txt and dv.txt files from Archivist Datasets, rename: prefix.tvlinking.txt, prefix.dv.txt
    - QV (Question Variables) qv.txt, rename prefix.qvmapping.txt
    - TQ (Topic Questions) tq.txt, rename prefix.tqlinking.txt
"""

from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from urllib.parse import urlsplit, urlunsplit

from mylib import get_driver, url_base, archivist_login, get_base_url



import pandas as pd
import requests
import time
import sys
import os

n = 5


def download_link(url_location, output_file):
    """
    Given a URL, using requests library to download it to output_file
    """
    print("Download: " + url_location)
    url_get = requests.get(url_location)
    url_get.raise_for_status()
    with open(output_file, "wb") as f: 
        f.write(url_get.content)


def archivist_download_txt(df, main_dir, uname, pw):
    """
    Loop over prefix, downloading txt files
    """
    df_base_url = get_base_url(df)
    export_name = pd.Series(df_base_url.base_url.values, index=df.Instrument).to_dict()
    print("Got {} prefix names".format(len(export_name)))
    print(export_name)

    def log_to_csv(prefix, dataset_name, dataset_id, text_list_file):
        """append a line to spreadsheet with three values"""
        with open(text_list_file, "a") as f:
            f.write( ",".join([prefix, dataset_name, dataset_id]) + "\n")

    k = -1
    pre_url = None
    for prefix, url in export_name.items():
        prefix = prefix.strip()

        split_url = urlsplit(url)

        output_prefix_dir = os.path.join(main_dir, split_url.netloc.split('.')[0], "txt_by_prefix")
        if not os.path.exists(output_prefix_dir):
            os.makedirs(output_prefix_dir)
        output_type_dir = os.path.join(main_dir, split_url.netloc.split('.')[0], "txt_by_type")
        if not os.path.exists(output_type_dir):
            os.makedirs(output_type_dir)

        text_list_file = os.path.join(os.path.dirname(output_prefix_dir), "text_list.csv")
        if not os.path.isfile(text_list_file) :
            with open(text_list_file, "a") as f:
                f.write( ",".join(["prefix", "instance name", "data link"]) + "\n")

        # download instrument: tqlinking, qvmapping
        # https://closer-archivist.herokuapp.com/admin/instruments/PREFIX/exports

        output_dir = os.path.join(output_prefix_dir, prefix)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        output_tq_dir = os.path.join(output_type_dir, "tqlinking",)
        if not os.path.exists(output_tq_dir):
            os.makedirs(output_tq_dir)

        output_qv_dir = os.path.join(output_type_dir, "qvmapping",)
        if not os.path.exists(output_qv_dir):
            os.makedirs(output_qv_dir)

        output_tv_dir = os.path.join(output_type_dir, "tvlinking",)
        if not os.path.exists(output_tv_dir):
            os.makedirs(output_tv_dir)

        output_dv_dir = os.path.join(output_type_dir, "dv",)
        if not os.path.exists(output_dv_dir):
            os.makedirs(output_dv_dir)

        k = k + 1
        print("Building a new driver")
        driver = get_driver()

        try:
            base_url = split_url.scheme + '://' + split_url.netloc
            print("Logging into host {}".format( base_url ))
            ok = archivist_login(driver, base_url, uname, pw)
            if not ok:
                print(f"Failed to login to {url}: skipping")
                continue
            print(f"Logged into prefix number {k}: {prefix}")

            print("Load 'Instrument Exports' page : " + url)
            driver.get(url)
            delay = 40*n   #seconds
            # find the input box
            inputElement = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//input[@placeholder='Search by prefix (press return to perform search)']")))
            print("Page is ready!")

            exports_page_url = os.path.join(os.path.dirname(url), prefix, "exports")
            print("Loading <exports> page for " + prefix)
            print(exports_page_url)
            driver.get(exports_page_url)
            delay = 5*n # seconds

            myEle = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//h2[text()='TQ']")))
            print("Instrument Export Page is ready!")

            # Topic Questions
            print("Getting tq.txt")
            tq_href = driver.find_element(By.XPATH,  "//*[contains(@href, 'tq.txt')]")
            tq_location = tq_href.get_attribute("href")

            out_tq_instrument = os.path.join(output_dir, prefix + ".tqlinking.txt")
            out_tq_type = os.path.join(output_tq_dir, prefix + ".tqlinking.txt")

            tq_get = requests.get(tq_location)
            tq_get.raise_for_status()
            with open(out_tq_instrument, "wb") as f:
                f.write(tq_get.content)
            with open(out_tq_type, "wb") as f:
                f.write(tq_get.content)

            # Question Variables
            print("Getting qv.txt")
            qv_href = driver.find_element(By.XPATH,  "//*[contains(@href, 'qv.txt')]")
            qv_location = qv_href.get_attribute("href")

            out_qv_instrument = os.path.join(output_dir, prefix + ".qvmapping.txt")
            out_qv_type = os.path.join(output_qv_dir, prefix + ".qvmapping.txt")

            qv_get = requests.get(qv_location)
            qv_get.raise_for_status()
            with open(out_qv_instrument, "wb") as f:
                f.write(qv_get.content)
            with open(out_qv_type, "wb") as f:
                f.write(qv_get.content)

            # Datasets:
            # find next table after h2
            print("look for datasets")
            try:
                data_table = driver.find_element(By.XPATH, "//h2[text()='Datasets']/following-sibling::table")
                #data_table.get_attribute('innerHTML')
                trs = data_table.find_elements(By.XPATH, 'tbody/tr')

                # find list of datasets
                for i in range(len(trs)):
                    instance_name = trs[i].find_element(By.XPATH, 'td').text
                    data_links = [(link.text, link.get_attribute('href')) for link in trs[i].find_elements(By.TAG_NAME, 'a')]
                    print(data_links)

                    # loop over datasets
                    if data_links == []:
                        # no dataset
                        print("no dataset found")

                    elif len(data_links) > 0:
                        data_link =[y for (x,y) in data_links if x=='VIEW'][0]
                        study_id = os.path.basename(data_link.split('/export')[0])

                        log_to_csv(prefix, instance_name, data_link, text_list_file)

                        # contains dataset tv/dv
                        for (data_type, data_link) in [(x,y) for (x,y) in data_links if x in ('tv.txt', 'dv.txt')]:
                            print("Getting " + study_id + ', ' + data_type)
                            content_get = requests.get(data_link)
                            content_get.raise_for_status()

                            if data_type == 'tv.txt':
                                type_name = '.tvlinking.txt'
                                output_dir_2 = output_tv_dir
                            elif data_type == 'dv.txt':
                                type_name = '.dv.txt'
                                output_dir_2 = output_dv_dir
                            else:
                                print('    ' + study_id + ', ' + data_type + ': this should not happen, check')

                            with open(os.path.join(output_dir, instance_name + type_name), "wb") as f:
                                f.write(content_get.content)

                            with open(os.path.join(output_dir_2, instance_name + type_name), "wb") as f:
                                f.write(content_get.content)

                    else:
                        print('data_links error')

            except NoSuchElementException:
                print("exception handled")

        except TimeoutException as e:
            print(f"Loading/downloading took too much time.  Error type: {type(e)}")
            print(e)
        finally:
            # succeeded or failed, we quit and make a new driver next iteration
            driver.quit()


def main():
    uname = sys.argv[1]
    pw = sys.argv[2]

    # Hayley's txt as dataframe
    df = pd.read_csv("Prefixes_to_export.txt", sep="\t")
    main_dir = "export_txt"

    # get all text files
    archivist_download_txt(df, main_dir, uname, pw)


if __name__ == "__main__":
    main()
